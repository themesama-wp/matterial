<?php
/**
 * The template for displaying Page Header
 *
 * @since Matterial 1.0
 */
?>

  <header class="page-header">

    <?php do_action('matterial_before_page_header'); ?>

    <div class="<?php echo apply_filters('matterial_page_header_container_class', 'container'); ?>">
      <div class="row">

        <div class="col-xs-12 page-header-standard">
          <h1 class="page-title"><?php the_title(); ?></h1>
          <!--<p class="page-description"><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', TS_TD ); ?></p>-->
        </div>

      </div>
    </div>

    <?php do_action('matterial_after_page_header'); ?>

  </header><!-- .page-header -->