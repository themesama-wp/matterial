<?php
/**
 * The template for displaying the header
 *
 *
 * @since Matterial 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width">

  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
  <![endif]-->
  <script>(function(){document.documentElement.className='js'})();</script>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php 
    /*
     * matterial_main_before hook
     * 
     * @hooked matterial_main_menu - 10
     * @hooked matterial_shop_sidebar - 12
     *
    */
    do_action('matterial_main_before');
  ?>

  <div id="main" class="site-content" role="main">

    <header class="site-header">
      <div class="container">
        <div class="pull-left">
          <a href="#" class="main-menu-btn header-btn"></a>
        </div>

        <?php 
          /*
           * matterial_header hook
           * 
           * @hooked matterial_breadcrumbs - 10
           * @hooked matterial_header_logo - 12
           *
          */
          do_action('matterial_header');
        ?>

        <div id="header-cart-btn" class="pull-right hidden hidden-xs">
          <a href="#" class="more-btn shop-menu-btn header-btn"></a>
        </div>
        <div class="pull-right">
          <a href="#" class="search-btn header-btn"></a>
        </div>
      </div>
    </header><!-- .site-header -->