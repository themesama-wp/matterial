<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @since Matterial 1.0
 */

get_header(); ?>
  
  <header class="page-header">

    <?php do_action('matterial_before_page_header'); ?>

    <div class="<?php echo apply_filters('matterial_page_header_container_class', 'container'); ?>">
      <div class="row">

        <div class="col-xs-12 page-header-standard">
          <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', TS_TD ); ?></h1>
          <p class="page-description"><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', TS_TD ); ?></p>
        </div>

      </div>
    </div>

    <?php do_action('matterial_after_page_header'); ?>

  </header><!-- .page-header -->

  <div class="page-content">
    <div class="container">
    <section class="error-404 not-found">
      <?php
        get_search_form();
        echo '<div class="t-404">'.__( '404!', TS_TD ).'</div>';
      ?>
    </section><!-- .error-404 -->
    </div>
  </div><!-- .page-content -->

<?php get_footer(); ?>