<?php
/**
 * The main template file
 *
 * @since Slupy 1.0
 */

get_header();
get_matterial_part( 'pageheader' ); ?>

  <div class="page-content">
    <div class="container">
        <?php
          // Start the loop.
          while ( have_posts() ) : the_post();

            // Include the page content template.
            the_content();

          // End the loop.
          endwhile;
        ?>
    </div>
  </div><!-- .page-content -->

<?php get_footer(); ?>