<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Themesama Framework Fields
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Field') ) {

class TS_Framework_Field {

  public $type = 'text';

  public $field = array();
  public $field_attrs = '';

  protected $field_output = '';
  
  /**
   * Constructor
   *
   * @since 1.0.1
   *
   * @param array $field
   */
  public function __construct( $field = array() ) {
    $this->field = $field;

    if( !empty( $this->field['type'] ) ) {
      $this->type = $this->field['type'];
    }

    $this->get_attributes();
    $this->check_default();
    $this->output();
  }

  /**
   * Field Render Container
   *
   * @since 1.0.1
   */
  public function output() {

    if( !empty( $this->field ) ) {
      $this->get_title();
      $this->render();
      $this->get_description();
    }

    echo !empty( $this->field_output ) ? $this->field_output : '';
  }

  /**
   * Field title output
   *
   * @since 1.0.1
   */
  public function get_title() {
    //title output
    if( !empty( $this->field['label'] ) ) {
      $this->field_output .= '<p><strong>'.esc_html( $this->field['label'] ).'</strong></p>';
    }

  }

  /**
   * Field Description
   *
   * @since 1.0.1
   */
  public function get_description() {
    //description output
    if( !empty( $this->field['description'] ) ) {
      $this->field_output .= '<p>'.$this->field['description'].'</p>';
    }

  }

  /**
   * Field Attributes
   *
   * @since 1.0.1
   */
  public function get_attributes() {
    //field extra attrs
    if( !empty( $this->field['name'] ) ) {
      $field_name = $this->field['name'];
    }else if( !empty( $this->field['id'] ) ) {
      $field_name = $this->field['id'];
    }

    if( !empty( $field_name ) ) {
      $this->add_attr( 'name', $field_name );
    }

    if( !empty( $this->field['attrs'] ) && is_array( $this->field['attrs'] ) ) {
      foreach ($this->field['attrs'] as $attr => $value) {
        $this->add_attr( $attr, $value );
      }
    }

  }

  /**
   * Field New Attribute
   *
   * @since 1.0.1
   *
   * @param string $attr
   * @param string $value
   * @param booelan $url 
   */
  public function add_attr( $attr = '', $value = '', $url = false ) {
    //check escape type
    if( !empty( $attr ) && isset( $value ) && !$url ) {
      $this->field_attrs .= $attr.'="'.esc_attr( $value ).'" ';
    }else if( !empty( $attr ) && !empty( $value ) && $url ) {
      $this->field_attrs .= $attr.'="'.esc_url( $value ).'" ';
    }

  }

  /**
   * Check Default Value
   *
   * @since 1.0.1
   */
  public function check_default() {
    //check default value
    if( !empty( $this->field['default'] ) && !isset( $this->field['value'] ) ) {
      //default value
      $this->field['value'] = $this->field['default'];
    }

  }

  /**
   * Render Field
   *
   * @since 1.0.1
   */
  public function render() {
    //check field type
    switch ( $this->type ) {
      case 'textarea':
        $this->field_output .= '<div class="ts-form-field"><textarea '.$this->field_attrs.'>'.( !empty( $this->field['value'] ) ? esc_textarea( $this->field['value'] ) : '' ).'</textarea></div>';
      break;

      case 'checkbox':
        //field extra attrs
        $this->add_attr( 'type', $this->type );
        //output
        $this->field_output .= '<div class="ts-form-field"><input '.$this->field_attrs.( !empty( $this->field['value'] ) ? checked( '1', $this->field['value'], false ) : '' ).'></div>';
      break;

      case 'radio':
        //field extra attrs
        $this->add_attr( 'type', $this->type );

        $this->field_output .= '<div class="ts-form-field type-radio">';

        if( !empty( $this->field['choices'] ) && is_array( $this->field['choices'] ) ) {
          foreach ($this->field['choices'] as $option_value => $option) {
            //output
            $this->field_output .= '<label><input value="'.esc_attr( $option_value ).'" '.$this->field_attrs.( !empty( $this->field['value'] ) ? checked( $option_value, $this->field['value'], false ) : '' ).'> '.esc_html( $option ).'</label>';
          }
        }

        $this->field_output .= '</div>';
      break;

      case 'select':
        $this->field_output .= '<div class="ts-form-field type-select">';
        $this->field_output .= '<select '.$this->field_attrs.'>';

        if( !empty( $this->field['choices'] ) && is_array( $this->field['choices'] ) ) {
          foreach ($this->field['choices'] as $option_value => $option) {
            //output
            $this->field_output .= '<option value="'.esc_attr( $option_value ).'" '.( !empty( $this->field['value'] ) ? selected( $option_value, $this->field['value'], false ) : '' ).'> '.esc_html( $option ).'</option>';
          }
        }

        $this->field_output .= '</select></div>';
      break;

      case 'description':
        $this->field_output .= '<div class="ts-form-field type-desc">';
        $this->field_output .= $this->field['default'];
        $this->field_output .= '</div>';
      break;
      
      default:
        //field extra attrs
        $this->add_attr( 'type', $this->type );
        if( !empty( $this->field['value'] ) ) {
          $this->add_attr( 'value', $this->field['value'] );
        }
        //output
        $this->field_output .= '<div class="ts-form-field"><input '.$this->field_attrs.'></div>';
      break;
    }

  }

  /**
   * Get Controls
   *
   * @since 1.0.1
   */
  public function get_field( $control = array() ) {
    $output = '';

    //control
    if( empty( $control['type'] ) ) {
      return '';
    }
    
    $field_class_name = $this->get_field_class( $control['type'] );

    ob_start();
    new $field_class_name( $control );
    $output = ob_get_contents();
    ob_end_clean();

    return !empty( $output ) ? $output : '';
  }

  /**
   * Get Controls Class Name
   *
   * @since 1.0.1
   */
  public function get_field_class( $class_name = '' ) {

    $class_name = str_replace( '-', ' ', $class_name );
    $class_name = 'TS_Framework_'.ucwords( $class_name ).'_Field';
    $class_name = str_replace( ' ', '_', $class_name);

    return class_exists($class_name) ? $class_name : 'TS_Framework_Field';
  }

}

}
