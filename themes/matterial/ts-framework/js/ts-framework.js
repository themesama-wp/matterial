;(function($, window, document, undefined) {
  'use strict';

  var $window = $(window);
  var $document = $(document);

  var windowHeight = $window.height();
  var is_device_mobile = (navigator.userAgent.match(/(Android|iPhone|iPad|iPod|Opera Mini|webOS|BlackBerry|IEMobile)/)) ? true : false;

  var TS_Framework_Uploader;

  $window.resize(function () {
    windowHeight = $window.height();
  });

  $.exists = function(selector) {
    return ($(selector).length > 0);
  };

  /*---------------------------------------------
    Group Field
  ---------------------------------------------*/
  $.fn.tsFrameworkGroup = function() {

    return this.each(function() {
      //variables
      var $this     = $(this),
          $holder   = $this.find('.ts-multi-group'),
          $sample   = $this.find('.ts-single-group.hidden'),
          $btn      = $this.find('.ts-group-new-btn'),
          _next_id  = $sample.data('max-id'),
          _group_id = $sample.data('group-id'),
          _field_name;

      $btn.click(function(e) {
        e.preventDefault();
        //add new clone
        $sample.clone().appendTo( $holder ).removeClass('hidden').removeAttr('data-max-id').removeAttr('data-group-id');
        //set name value
        $('[data-field-id]', $holder).each(function() {
          _field_name = _group_id + '[' + _next_id + ']' + $(this).data('field-id');
          
          $(this).attr('name', _field_name).removeAttr('data-field-id');
          //switch type
          if( $(this).is(':checkbox') ) {
            $(this).attr('id', _field_name).next().attr('for', _field_name);
          }
        });
        //trigger
        $( '.ts-form-field.type-color input[type="text"]', $holder ).tsFrameworkColorPicker();
        $( '.ts-form-field.type-upload', $holder ).tsFrameworkUploader();
        //next id
        _next_id++;
      });

    });

  };

  //Toggle, Change Title & Remove Group Item
  $( 'body' ).on('click', '.ts-multi-group .ts-group-title', function() {
    $(this).toggleClass('active-toggle').next().slideToggle();

  }).on('click', '.ts-multi-group .ts-group-remove-btn', function(e) {
    e.preventDefault();
    $(this).parent().slideUp(300, function() {
      $(this).parents('.ts-single-group').remove();
    });

  }).on('keyup','.ts-single-group-content .ts-form-field:nth-child(2) input[type="text"]', function() {
    var $this = $(this),
        $title = $this.parents('.ts-single-group').find('.ts-group-title');

    if($this.val() !== ''){
      $title.text( $this.val() );
    }else{
      $title.text( $title.data('placeholder') );
    }

  });

  //Sortable Group
  $( '.ts-multi-group' ).sortable({ 
    axis: 'y',
    handle: '.ts-group-title',
    start: function( event, ui ) {
      $('.ts-single-group-content', this).hide();
      $('.active-toggle', this).removeClass('active-toggle');
      ui.placeholder.height($('.ts-group-title', ui.item.context).outerHeight());
      $(this).sortable( 'refreshPositions' );
    }
  });

  /*---------------------------------------------
    Media Upload
  ---------------------------------------------*/
  $.fn.tsFrameworkUploader = function() {

    return this.each(function() {
      //variables
      var $this       = $(this),
          $input      = $this.find('input[type="text"]'),
          $btn        = $this.find('.ts-upload-btn'),
          _title      = $input.data('title') || '',
          _btn_title  = $input.data('btn-title') || '',
          _file_type  = $input.data('file-type') || '';

      $btn.on('click', function(e) {
        e.preventDefault();
 
        //Extend the wp.media object
        TS_Framework_Uploader = wp.media.frames.file_frame = wp.media({
          title: _title,
          button: {
            text: _btn_title
          },
          library:{
            type: _file_type
          },
          multiple: false
        });
 
        //When a file is selected, grab the URL and set it as the text field's value
        TS_Framework_Uploader.on('select', function() {
          var selection = TS_Framework_Uploader.state().get('selection');
          
          selection.map( function( attachment ) {

            attachment = attachment.toJSON();

            $input.val(attachment.url).change();
       
          });

        });
 
        //Open the uploader dialog
        TS_Framework_Uploader.open();
 
      });

    });

  };

  /*---------------------------------------------
    Color Picker
  ---------------------------------------------*/
  $.fn.tsFrameworkColorPicker = function() {

    return this.each(function() {
      //variables
      var $this = $(this);

      if ( typeof $.fn.wpColorPicker == 'function' && !$this.hasClass('wp-color-picker') && !$this.attr('data-field-id') ) {
        $this.wpColorPicker();
      }

    });

  };

  /*---------------------------------------------
    Meta Boxes
  ---------------------------------------------*/
  $.fn.tsFrameworkMetaBoxes = function() {

    return this.each(function() {
      //variables
      var $this     = $(this),
          $tab      = $this.find('.ts-meta-tab-link'),
          $content  = $this.find('.ts-meta-tabs-content'),
          $tabs     = $content.find('.ts-meta-tab'),
          _current  = $tab.first().data('metatab-id'),
          _id       = $this.data('id');

      if( $.cookie( _id ) && !$content.hasClass('ts-meta-tab-single') ) {
        //current tab
        _current = $.cookie( _id );
      }else if( $content.hasClass('ts-meta-tab-single') ) {
        _current = $tabs.first().data('metatab-id');
      }

      //open current tab
      $tabs.hide();
      $content.find('.ts-meta-tab[data-metatab-id="'+ _current +'"]').show();
      $this.find('.ts-meta-tab-link[data-metatab-id="'+ _current +'"]').addClass('current-tab');

      //change tab
      $tab.click(function(e) {
        e.preventDefault();

        if( _current != $(this).data('metatab-id') ) {
          $tab.removeClass('current-tab');
          $(this).addClass('current-tab');

          _current = $(this).data('metatab-id');
          $.cookie( _id, _current );

          $tabs.hide();
          $content.find('.ts-meta-tab[data-metatab-id="'+ _current +'"]').show();

        }

      });

    });

  };

  /*---------------------------------------------
    Framework Menu
  ---------------------------------------------*/
  $.fn.tsFrameworkMenu = function() {

    return this.each(function() {
      //variables
      var $this     = $(this),
          $heading  = $this.find('.ts-fw-heading'),
          $menu     = $this.find('.ts-framework-menu'),
          $main_tab = $this.find('.ts-fw-main-tab'),
          $sub_tab  = $this.find('.ts-fw-sub-tab'),
          $tabs     = $this.find('.ts-fw-tabs'),
          $tab      = $this.find('.ts-fw-tab'),
          _heading  = $sub_tab.first().html(),
          _current  = $sub_tab.first().data('fwtab-id');
          
      if( $.cookie( 'ts-fw-current' ) && $.cookie( 'ts-fw-heading' ) ) {
        //current tab
        _current = $.cookie( 'ts-fw-current' );
        _heading = $.cookie( 'ts-fw-heading' );
      }

      $tabs.find('.ts-fw-tab[data-fwtab-id="'+ _current +'"]').show();
      $main_tab.find('.ts-fw-sub-tab[data-fwtab-id="'+ _current +'"]').addClass('current-tab').parents('.ts-fw-main-tab').addClass('current-tab').find('ul').slideDown();
      $heading.html(_heading);

      //main tab
      $('> span', $main_tab).click(function() {
        var $parent = $(this).parent();

        //check single tab
        if( $parent.hasClass('ts-fw-single-tab') && !$parent.hasClass('current-tab') ) {
          $menu.find('.current-tab', $main_tab).removeClass('current-tab').find('ul').slideUp();
          $parent.addClass('current-tab').find('.ts-fw-sub-tab').first().trigger('click');

        }else if( $parent.hasClass('current-tab') && !$parent.hasClass('ts-fw-single-tab') ) {
          $parent.removeClass('current-tab').find('ul').slideUp();

        }else if( !$parent.hasClass('ts-fw-single-tab') ){
          $menu.find('.current-tab', $main_tab).removeClass('current-tab').find('ul').slideUp();
          $parent.addClass('current-tab').find('ul').slideDown().find('.ts-fw-sub-tab').first().trigger('click');

        }

      });

      //sub tab
      $sub_tab.click(function(e) {
        e.preventDefault();

        if( _current != $(this).data('fwtab-id') ) {
          $(this).addClass('current-tab').siblings().removeClass('current-tab');

          //set current
          _current = $(this).data('fwtab-id');
          $.cookie( 'ts-fw-current', _current );
          
          //set heading
          _heading = $(this).html();
          $heading.html(_heading);
          $.cookie( 'ts-fw-heading', _heading );

          //open current tab
          $tab.hide();
          $tabs.find('.ts-fw-tab[data-fwtab-id="'+ _current +'"]').show();

        }

      });

    });

  };

  $('body').on('keyup', 'input[data-cl]', function() {
    var $this = $(this),
        $counter = $this.parent().next().find('.input-counter'),
        _text = $this.data('cl'),
        _length = $this.val().length,
        _max  = $this.attr('maxlength');

    _text = _text.replace( '%s', (_max - _length) );

    $counter.html(_text);

  }).on('change', 'input[data-cl]', function() {
    $(this).trigger('keyup');
  });

  /*---------------------------------------------
    Call Plugins
  ---------------------------------------------*/
  $(document).ready(function(){
    
    $( '.ts-form-field.type-color input[type="text"]' ).tsFrameworkColorPicker();
    $( '.ts-form-field.type-upload' ).tsFrameworkUploader();
    $( '.ts-form-field.type-group' ).tsFrameworkGroup();
    $( '.ts-meta-boxes' ).tsFrameworkMetaBoxes();
    $( '#ts-framework-form' ).tsFrameworkMenu();

  });

}(jQuery, window, document));