<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
* Themesama Framework Settings
*
* @since 1.0.1
*/
if( !class_exists('TS_Framework_Settings') && class_exists('TS_Framework') ) {

class TS_Framework_Settings extends TS_Framework {

  public $theme_framework_tabs = array();
  public $theme_framework_menu = '';
  
  /**
   * Constructor
   *
   * @since 1.0.1
   */
  function __construct() {
    //all settings
    $this->theme_framework_tabs = apply_filters( 'ts_framework_tabs', array() );

    //admin init
    $this->add_action( 'admin_init', 'admin_init' );

    //admin menu
    $this->add_action( 'admin_menu', 'add_theme_page' );
  }

  /**
   * Admin Init
   *
   * @since 1.0.1
   */
  public function admin_init() {
    //register settings
    register_setting( TS_OPTION, TS_OPTION, array( $this, 'save_framework' ) );

    //get controls & tabs
    $this->theme_framework_menu .= '<ul class="ts-framework-menu">';

    foreach ($this->theme_framework_tabs as $maintab_id => $main_tab) {
      //control
      if( empty( $main_tab['label'] ) || empty( $main_tab['tabs'] ) ) {
        continue;
      }
      //main tab
      $this->theme_framework_menu .= '<li class="ts-fw-main-tab'.(count( $main_tab['tabs'] ) < 2 ? ' ts-fw-single-tab' : '').'"><span>'.$main_tab['label'].'</span><ul>';

      //sub tabs
      foreach ($main_tab['tabs'] as $subtab_id => $sub_tab) {
        //control
        if( empty( $sub_tab['id'] ) || empty( $sub_tab['label'] ) || empty( $sub_tab['controls'] ) ) {
          continue;
        }
        //sub tab
        $this->theme_framework_menu .= '<li class="ts-fw-sub-tab" data-fwtab-id="'.esc_attr( $sub_tab['id'] ).'">'.$sub_tab['label'].'</li>';

        //settings section
        add_settings_section( $sub_tab['id'], $sub_tab['label'], array( $this, 'setting_section' ), TS_OPTION );

        //controls
        foreach ($sub_tab['controls'] as $control_id => $control) {
          //control
          if( empty( $control['id'] ) || empty( $control['label'] ) || empty( $control['type'] ) ) {
            continue;
          }

          //add settings field
          add_settings_field( $sub_tab['id'].$control['id'], $control['label'], array( $this, 'get_field' ), TS_OPTION, $sub_tab['id'], $control );
        }
      }

      //main tab close
      $this->theme_framework_menu .= '</ul></li>';
    }

    $this->theme_framework_menu .= '</ul>';
  }

  /**
   * Save Framework Settings
   *
   * @since 1.0.1
   */
  public function save_framework( $settings ) {
    $framework_notice = __( 'Settings Saved.', TS_TD );
    $framework_notice_type = 'updated';
    $settings_ok = true;

    //sanitize values
    foreach ($this->theme_framework_tabs as $maintab_id => $main_tab) {
      //control
      if( empty( $main_tab['tabs'] ) ) {
        continue;
      }

      foreach ($main_tab['tabs'] as $subtab_id => $sub_tab) {
        //control
        if( empty( $sub_tab['controls'] ) ) {
          continue;
        }

        //controls
        foreach ($sub_tab['controls'] as $control_id => $control) {
          //sanitize field
          $framework_field_value = isset( $settings[ $control['id'] ] ) ? $settings[ $control['id'] ] : '';
          $settings[ $control['id'] ] = $this->sanitize_field( $control['type'], $framework_field_value, $control );
        }
      }
    }

    add_settings_error( 'ts-framework-notice', 'ts-notice', $framework_notice, $framework_notice_type );

    //check settings
    if( $settings_ok ) {
      return $settings;
    }else {
      return;
    }

  }

  /**
   * Setting Section
   *
   * @since 1.0.1
   */
  public function setting_section() {}

  /**
   * Add Theme Settings Page
   *
   * @since 1.0.1
   */
  public function add_theme_page() {
    //mode
    $menu_settings = apply_filters( 'ts_framework_menu', array(
      'mode'        => 'theme',
      'title'       => '',
      'position'    => '',
      'parent_slug' => 'options-general.php'
    ));

    //check title
    if( empty( $menu_settings['title'] ) ) {
      $menu_settings['title'] = __( 'Theme Options', TS_TD );
    }

    //check mode
    if( !empty( $menu_settings['mode'] ) && !empty( $this->theme_framework_tabs ) ) {
      switch ( $menu_settings['mode'] ) {
        case 'sub':
          //control
          if( empty( $menu_settings['parent_slug'] ) ) {
            $menu_settings['parent_slug'] = 'options-general.php';
          }
          //add submenu page
          add_submenu_page( $menu_settings['parent_slug'], 'Themesama Framework', $menu_settings['title'], 'manage_options', TS_OPTION, array( $this, 'init_framework' ) );
        break;
        
        case 'main':
          //control
          if( empty( $menu_settings['position'] ) ) {
            $menu_settings['position'] = '';
          }
          //add menu page
          add_menu_page( 'Themesama Framework', $menu_settings['title'], 'manage_options', TS_OPTION, array( $this, 'init_framework' ), '', $menu_settings['position'] );
        break;

        default:
          //add theme page
          add_theme_page( 'Themesama Framework', $menu_settings['title'], 'manage_options', TS_OPTION, array( $this, 'init_framework' ) );
        break;
      }
    }

  }

  /**
   * Create Theme Settings Page
   *
   * @since 1.0.1
   */
  public function init_framework() {
    //output
    echo '<div class="wrap">'.$this->get_notices().'<form action="options.php" method="post" id="ts-framework-form">';
    settings_fields(TS_OPTION);
    $this->get_framework_header();
    echo '<div class="ts-fw-content">';
    echo !empty( $this->theme_framework_menu ) ? $this->theme_framework_menu : '';
    echo '<div class="ts-fw-tabs">';
    $this->do_settings_sections(TS_OPTION);
    echo '</div></div>';
    echo  '</form></div>';

  }

  /**
   * Framework Header
   *
   * @since 1.0.1
   */
  public function get_framework_header() {
    echo '<div class="ts-fw-header">';
    echo '<div class="ts-fw-logo">'.apply_filters( 'ts_framework_logo', '<img src="'.esc_url( TS_FW_IMG.'/logo.png' ).'" alt="logo">' ).'</div>';
    echo '<h1 class="ts-fw-heading"></h1>';
    submit_button('', 'primary large ts-option-button', 'submit', '');
    echo '</div>';
  }

  /**
   * Framework Notices
   *
   * @since 1.0.1
   */
  public function get_notices() {
    //output
    $output = '';

    //get all notices
    $all_notices = get_settings_errors( 'ts-framework-notice' );

    //check
    if ( is_array( $all_notices ) && count( $all_notices ) > 0 && !empty( $all_notices[0]['message'] ) && !empty( $all_notices[0]['type'] ) ) {
      $message = $all_notices[0]['message'];
      $type = $all_notices[0]['type'];

      $output = '<div class="ts-framework-notice '.esc_attr( $type ).' settings-error"><p>'.$message.'</p></div>';
    }

    return $output;
  }

  /**
   * Create Settings Sections
   *
   * @since 1.0.1
   */
  private function do_settings_sections( $page ){
    //variables
    global $wp_settings_sections, $wp_settings_fields;

    //control
    if ( ! isset( $wp_settings_sections[$page] ) ) {
      return;
    }


    foreach ( (array) $wp_settings_sections[$page] as $section ) {
      //callback section
      if ( $section['callback'] ) {
        call_user_func( $section['callback'], $section );
      }

      //control
      if ( !isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section['id']] ) ) {
        continue;
      }

      echo '<div class="ts-fw-tab" data-fwtab-id="'.esc_attr( $section['id'] ).'">';
        $this->do_settings_fields( $page, $section['id'] );
      echo '</div>';

    }

  }

  /**
   * Create Settings Section's Fields
   *
   * @since 1.0.1
   */
  private function do_settings_fields( $page, $section ) {
    //variable
    global $wp_settings_fields;

    //control
    if ( !isset( $wp_settings_fields[$page][$section] ) ) {
      return;
    }

    foreach ( (array) $wp_settings_fields[$page][$section] as $field ) {
      //control
      if( empty( $field['args']['name'] ) && !empty( $field['args']['id'] ) ) {
        $field['args']['name'] = TS_OPTION.'['.$field['args']['id'].']';
      }
      //get value
      if( !isset( $field['args']['value'] ) ){
        $field['args']['value'] = ts_get_option($field['args']['id']);
      }

      echo call_user_func( $field['callback'], $field['args'] );
    }

  }


}

}

new TS_Framework_Settings();