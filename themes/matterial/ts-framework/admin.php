<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Themesama Framework
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework') ) {

class TS_Framework {

  public $theme_option_name = '';
  public $version = '1.0.1';
  
  /**
   * Constructor
   *
   * @since 1.0.1
   */
  function __construct() {
    //Apply filters
    $this->filters();

    // Define constants
    $this->define_constants();

    // Include required files
    $this->includes();

    //style & script
    $this->add_action( 'admin_enqueue_scripts', 'ts_admin_enqueue_scripts' );

  }

  /**
   * Define TS Framework Constants
   *
   * @since 1.0.1
   */
  private function define_constants() {
    defined('TS_PATH')    || define( 'TS_PATH', get_template_directory() );
    defined('TS_FW_PATH') || define( 'TS_FW_PATH', TS_PATH.'/ts-framework' );

    defined('TS_URI')     || define( 'TS_URI', get_template_directory_uri() );
    defined('TS_IMG')     || define( 'TS_IMG', TS_URI.'/images' );
    defined('TS_JS')      || define( 'TS_JS', TS_URI.'/js' );
    defined('TS_CSS')     || define( 'TS_CSS', TS_URI.'/css' );

    $wp_get_theme = wp_get_theme();

    defined('TS_VER')     || define( 'TS_VER', $wp_get_theme->get( 'Version' ) );
    defined('TS_TD')      || define( 'TS_TD', $wp_get_theme->get( 'TextDomain' ) );

    defined('TS_FW_URI')  || define( 'TS_FW_URI', TS_URI.'/ts-framework' );
    defined('TS_FW_IMG')  || define( 'TS_FW_IMG', TS_FW_URI.'/images' );
    defined('TS_FW_JS')   || define( 'TS_FW_JS', TS_FW_URI.'/js' );
    defined('TS_FW_CSS')  || define( 'TS_FW_CSS', TS_FW_URI.'/css' );

    defined('TS_FW_VER')  || define( 'TS_FW_VER', $this->version );
    defined('TS_OPTION')  || define( 'TS_OPTION', $this->theme_option_name );

    defined('TS_MIN')     || define( 'TS_MIN', '.min' );
  }

  /**
   * Include required files.
   *
   * @since 1.0.1
   */
  private function includes() {
    //Load Fields
    require_once( TS_FW_PATH.'/ts-fields.php' );
    //Load Custom Framework Fields
    $all_framework_fields = apply_filters( 'ts_framework_custom_fields', array('switch', 'image-switch', 'group', 'color-picker', 'upload') );
    foreach ($all_framework_fields as $key => $field_name) {
      require_once( TS_FW_PATH.'/fields/ts-'.$field_name.'.php' );
    }

    //Load Framework
    require_once( TS_FW_PATH.'/ts-customization.php' );
    require_once( TS_FW_PATH.'/ts-meta.php' );
    require_once( TS_FW_PATH.'/ts-theme-options.php' );
  }

  /**
   * Apply filters
   *
   * @since 1.0.1
   */
  public function filters() {
    $this->theme_option_name = apply_filters( 'ts_framework_theme_option_name', 'ts_framework' );
  }

  /**
   * Add action
   *
   * @since 1.0.1
   */
  public function add_action( $tag = '', $function_name = '' ) {
    add_action( $tag, array( $this, $function_name ) );
  }

  /**
   * Enqueue Style & Scripts
   *
   * @since 1.0.1
   */
  public function ts_admin_enqueue_scripts() {
    //style
    wp_enqueue_style( 'ts-framework', TS_FW_CSS.'/ts-framework'.TS_MIN.'.css', array(), TS_FW_VER );
    
    //script
    wp_enqueue_script( 'jquery-cookie', TS_FW_JS.'/jquery.cookie.js', array('jquery'), '1.4.1', true );
    wp_enqueue_script( 'ts-framework', TS_FW_JS.'/ts-framework'.TS_MIN.'.js', array('jquery'), TS_FW_VER, true );

    //color picker
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_style( 'wp-color-picker' );

    //media
    wp_enqueue_media();

    //sortable
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-sortable' );
  }

  /**
   * Get Controls
   *
   * @since 1.0.1
   */
  public function get_field( $control = array() ) {
    $output = '';

    //control
    if( empty( $control['type'] ) ) {
      return '';
    }
    
    $field_class_name = $this->get_field_class( $control['type'] );

    ob_start();
    new $field_class_name( $control );
    $output = ob_get_contents();
    ob_end_clean();

    return !empty( $output ) ? $output : '';
  }

  /**
   * Sanitize Fields
   *
   * @since 1.0.1
   */
  public function sanitize_field( $type = 'text', $value = '', $field = array() ) {
    $output = '';

    //check type
    switch ($type) {
      case 'switch':
      case 'checkbox':
        if( empty( $value ) ) {
          $output = '0';
        }else {
          $output = '1';
        }
      break;

      case 'url':
      case 'upload':
        $output = esc_url_raw( $value );
      break;

      case 'background':
        if( is_array( $value ) ) {
          $output = $value;
        }
      break;

      case 'group':
        if( is_array( $value ) && !empty( $field['controls'] ) ) {
          foreach ($value as $group_key => $group) {
            //
            foreach ($field['controls'] as $control_id => $control) {
              $framework_field_value = isset( $value[$group_key][ $control['id'] ] ) ? $value[$group_key][ $control['id'] ] : '';
              $value[$group_key][ $control['id'] ] = $this->sanitize_field( $control['type'], $framework_field_value );
            }
            
          }

          $output = $value;
        }
      break;
      
      case 'textarea':
        $output = $value;
      break;
      
      default:
        $output = sanitize_text_field( $value );
      break;
    }

    return $output;
  }

  /**
   * Get Controls Class Name
   *
   * @since 1.0.1
   */
  public function get_field_class( $class_name = '' ) {

    $class_name = str_replace( '-', ' ', $class_name );
    $class_name = 'TS_Framework_'.ucwords( $class_name ).'_Field';
    $class_name = str_replace( ' ', '_', $class_name);

    return class_exists($class_name) ? $class_name : 'TS_Framework_Field';
  }

}

new TS_Framework();

}


/**
 * Get Option.
 *
 * Helper function to return the option value.
 *
 * @param     string    The option ID.
 * @return    mixed
 *
 * @access    public
 * @since     1.0
 */
if ( !function_exists( 'ts_get_option' ) ) {

function ts_get_option( $option_id ) {
  
  global $TS_OPTIONS;
  
  if( !isset($TS_OPTIONS) ){
    $TS_OPTIONS = get_option( TS_OPTION );
    
    if( !is_array($TS_OPTIONS)  ){
      $TS_OPTIONS = ts_check_default();
    }
  }

  if ( !empty( $TS_OPTIONS[$option_id] ) ) {
    return $TS_OPTIONS[$option_id];
  }else{
    return '';
  }
  
}
  
}

if ( !function_exists( 'ts_check_default' ) ) {

function ts_check_default() {
  $settings = apply_filters( 'ts_framework_tabs', array() );

  $settings_new = array();

  foreach ($settings as $setting_id => $setting) {
    foreach ($setting['tabs'] as $tab_id => $tab) {
      foreach ($tab['controls'] as $control_id => $control) {
        if( !empty( $control['default'] ) ) {
          $settings_new[$control['id']] = $control['default'];
        }
      }
    }
  }

  if( !empty( $settings_new ) ) {
    update_option( TS_OPTION, $settings_new );
  }
}

}