<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Group Field
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Group_Field') && class_exists('TS_Framework_Field') ) {

class TS_Framework_Group_Field extends TS_Framework_Field {
  
  public $type = 'group';

  /**
   * Render Field
   *
   * @since 1.0.1
   */
  public function render(){
    //control
    if( empty( $this->field['controls'] ) || !is_array( $this->field['controls'] ) ) {
      return;
    }

    //wrap
    $this->field_output .= '<div class="ts-form-field type-group">';

    //check saved values & create dynamic fields
    $max_id = 0;
    $this->field_output .= '<div class="ts-multi-group">';
    if( !empty( $this->field['value'] ) && is_array( $this->field['value'] ) ) {
      //get saved values
      foreach ($this->field['value'] as $field_id => $field) {
        $this->field_output .= '<div class="ts-single-group">';

        //get single title
        $single_custom_title = !empty( $field[ $this->field['controls'][0]['id'] ] ) ? $field[ $this->field['controls'][0]['id'] ] : __( 'Item #', TS_TD ).$field_id;
        $this->get_single_title($single_custom_title);
        
        //single content
        $this->field_output .= '<div class="ts-single-group-content">';
        
        foreach ($this->field['controls'] as $control_id => $control) {
          //check max_id
          if( $field_id > $max_id ) {
            $max_id = $field_id;
          }
          //check
          if( isset( $field[ $control['id'] ] ) ) {
            $control['value'] = $field[ $control['id'] ];
            $control['name'] = $this->field['name'].'['.$field_id.']['.$control['id'].']';
            //get field
            $this->field_output .= $this->get_field( $control );
          }
        }

        //remove btn
        $this->get_remove_btn();
        $this->field_output .= '</div></div>';
      }
    }
    $this->field_output .= '</div>';

    //create default fields
    $this->field_output .= '<div class="ts-single-group hidden" data-group-id="'.esc_attr( $this->field['name'] ).'" data-max-id="'.esc_attr( $max_id + 1 ).'">';
    
    //get single title
    $single_default_title = __( 'New', TS_TD );
    $this->get_single_title( $single_default_title );
    //single content
    $this->field_output .= '<div class="ts-single-group-content">';
    foreach ($this->field['controls'] as $control_id => $control) {
      //extra attr
      if( !empty( $control['id'] ) ) {
        $control['attrs']['data-field-id'] = '['.$control['id'].']';
        $control['id'] = '';
      }

      //get field
      $this->field_output .= $this->get_field( $control );
    }

    //remove btn
    $this->get_remove_btn();
    $this->field_output .= '</div></div>';

    //add new button
    $this->field_output .= '<a href="#" class="ts-group-new-btn button button-primary">'.__( 'Add New', TS_TD ).'</a>';

    $this->field_output .= '</div>';
  }

  /**
   * Single Group Item Title
   *
   * @since 1.0.1
   */
  public function get_single_title( $title = 'New' ) {
    $this->field_output .= '<div class="ts-group-title" data-placeholder="'.esc_attr( $title ).'">'.esc_html( $title ).'</div>';
  }

  /**
   * Remove Button
   *
   * @since 1.0.1
   */
  public function get_remove_btn() {
    $this->field_output .= '<a href="#" class="ts-group-remove-btn">'.esc_html__( 'Remove', TS_TD ).'</a>';
  }

}

}