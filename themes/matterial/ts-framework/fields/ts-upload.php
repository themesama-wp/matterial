<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Upload Field
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Upload_Field') && class_exists('TS_Framework_Field') ) {

class TS_Framework_Upload_Field extends TS_Framework_Field {
  
  public $type = 'text';

  /**
   * Render Field
   *
   * @since 1.0.1
   */
  public function render(){
    //field extra attrs
    $this->add_attr( 'type', 'text' );
    if( !empty( $this->field['value'] ) ) {
      $this->add_attr( 'value', $this->field['value'] );
    }

    if( !empty( $this->field['attrs']['data-btn-title'] ) ) {
      $button_title = $this->field['attrs']['data-btn-title'];
    }else {
      $button_title = esc_html__( 'Choose', TS_TD );
    }
    //output
    $this->field_output .= '<div class="ts-form-field type-upload"><input '.$this->field_attrs.'> <a href="#" class="ts-upload-btn button button-primary">'.$button_title.'</a></div>';

  }

}

}