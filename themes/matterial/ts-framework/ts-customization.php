<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Themesama Framework Customization
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Customization') ) {

class TS_Framework_Customization extends TS_Framework {

  public $theme_customization_tabs = array();
  
  /**
   * Constructor
   *
   * @since 1.0.1
   */
  function __construct() {
    $this->add_action( 'customize_register', 'customize_register' );
  }

  /**
   * Register Customization Tabs & Controls
   *
   * @since 1.0.1
   */
  public function customize_register( $wp_customize ) {
    //all customization options
    $this->theme_customization_tabs = apply_filters( 'ts_framework_customization_tabs', array() );

    //create customization tabs and settings
    foreach ($this->theme_customization_tabs as $tab_id => $tab) {
      //check values
      if( empty( $tab['options'] ) || empty( $tab['id'] ) || empty( $tab['controls'] ) ) {
        continue;
      }

      //add section
      $wp_customize->add_section( $tab['id'] , $tab['options']);

      //add settings & controls
      foreach ($tab['controls'] as $control_id => $control) {
        //check values
        if( empty( $control['id'] ) || empty( $control['type'] ) ) {
          continue;
        }

        //setting extra config 
        $settings_extra = array();

        //check default value
        if( !empty($control['default']) ) {
          $settings_extra['default'] = $control['default'];
        }else if( $control['type'] == 'color' ) {
          $settings_extra['default'] = '#000000';
        }

        //check priority
        if( !empty($control['priority']) ) {
          $settings_extra['priority'] = $control['priority'];
        }

        //check sanitize function
        if( !empty($control['sanitize_callback']) && function_exists($control['sanitize_callback']) ) {
          $settings_extra['sanitize_callback'] = $control['sanitize_callback'];
        }else {
          switch ( $control['type'] ) {
            case 'email':
              $settings_extra['sanitize_callback'] = 'sanitize_email';
            break;
            case 'url':
              $settings_extra['sanitize_callback'] = 'esc_url_raw';
            break;
            case 'color':
              $settings_extra['sanitize_callback'] = 'sanitize_hex_color';
            break;
          }
        }

        //add setting
        $wp_customize->add_setting( $control['id'], $settings_extra );

        //add control
        $control['section'] = $tab['id'];
        $control['settings'] = $control['id'];
        
        switch ( $control['type'] ) {
          case 'switch':
            $wp_customize->add_control( new TS_Customize_Switch_Control( $wp_customize, $control['id'], $control ) );
          break;
          case 'image-switch':
            $wp_customize->add_control( new TS_Customize_Image_Control( $wp_customize, $control['id'], $control ) );
          break;
          case 'color':
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $control['id'], $control ) );
          break;
          case 'upload':
            $wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, $control['id'], $control ) );
          break;
          case 'image':
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, $control['id'], $control ) );
          break;
          default:
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, $control['id'], $control ) );
          break;
        }

      } //add settings & controls foreach
      
    } //tabs foreach

  } // customize_register function

}

}

new TS_Framework_Customization();

/**
 * Switch Control
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Customize_Switch_Control') && class_exists('WP_Customize_Control') ) {

class TS_Customize_Switch_Control extends WP_Customize_Control {
  
  public $type = 'checkbox';

  /**
   * Render Content
   *
   * @since 1.0.1
   */
  public function render_content() {
    ?>
    <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
    <div class="ts-switch">
      <input id="<?php echo esc_attr( $this->id ); ?>" type="checkbox" value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); checked( $this->value() ); ?> />
      <label for="<?php echo esc_attr( $this->id ); ?>"></label>
    </div>
    <?php if ( ! empty( $this->description ) ) { ?>
      <span class="description customize-control-description"><?php echo $this->description; ?></span>
    <?php }
  }
}

}

/**
 * Image Control
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Customize_Image_Control') && class_exists('WP_Customize_Control') ) {

class TS_Customize_Image_Control extends WP_Customize_Control {
  
  public $type = 'radio';
  
  /**
   * Render Content
   *
   * @since 1.0.1
   */
  public function render_content() {

    if ( empty( $this->choices ) ) {
      return;
    }

    $name = '_customize-radio-' . $this->id;

    if ( !empty( $this->label ) ) { ?>
      <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
    <?php }
    if ( !empty( $this->description ) ) { ?>
      <span class="description customize-control-description"><?php echo $this->description ; ?></span>
    <?php }
    $i = 0;
    foreach ( $this->choices as $value => $label ) {
      $i++;
      ?>
      <div class="ts-image-switch">
        <input id="<?php echo esc_attr( $this->id ).$i; ?>" type="radio" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $name ); ?>" <?php $this->link(); checked( $this->value(), $value ); ?> />
        <label for="<?php echo esc_attr( $this->id ).$i; ?>"><?php echo !empty( $label ) ? '<img src="'.esc_url( $label ).'" alt="'.esc_attr( $value ).'">' : ''; ?></label>
      </div>
      <?php }
  }

}

}