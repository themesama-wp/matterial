<?php
/**
 * Matterial Theme Hooks
 *
 * @since Matterial 1.0
 * @author ThemeSama (@theme_sama)
 *
*/

//Theme Support
add_action( 'after_setup_theme', 'matterial_theme_support' );

//Styles & Scripts
add_action( 'wp_enqueue_scripts', 'matterial_enqueue_scripts' );

//Page Header
//add_action( 'matterial_before_page_header', 'matterial_before_page_header' );
add_action( 'matterial_after_page_header', 'matterial_after_page_header' );

//Header
add_action( 'matterial_header', 'matterial_breadcrumbs', 10 );
add_action( 'matterial_header', 'matterial_header_logo', 12 );

//Left & Right Side Menu
add_action( 'matterial_main_before', 'matterial_main_menu', 10 );
add_action( 'matterial_main_before', 'matterial_shop_sidebar', 12 );

//Yoast SEO Breadcrumb Separator
add_action( 'wpseo_breadcrumb_separator', 'matterial_breadcrumbs_separator' );

/*
 * WooCommerce Filter & Actions
 *
*/