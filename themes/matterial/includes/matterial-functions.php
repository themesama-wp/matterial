<?php
/**
 * Matterial Theme Functions
 *
 * @since Matterial 1.0
 * @author ThemeSama (@theme_sama)
 *
*/

/**
 * Theme Support, Nav & Textdomain
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if( !function_exists( 'matterial_theme_support' ) ) {
function matterial_theme_support() {
  //Site Title Tag
  add_theme_support( 'title-tag' );

  //Post Formats
  add_theme_support( 'post-formats', array(
    'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'
  ));

  //Post Thumbnails
  add_theme_support( 'post-thumbnails', array(
    'post', 'page'
  ));

  //Feed Links
  add_theme_support( 'automatic-feed-links' );

  //HTML5
  add_theme_support( 'html5', array(
    'comment-list', 'comment-form', 'search-form', 'gallery', 'caption'
  ));

  //Optional WooCommerce
  add_theme_support( 'woocommerce' );

  //Register Nav Menus
  register_nav_menus( array(
    'main'    => __('Main Menu', TS_TD ),
    'mobile'  => __('Mobile Menu', TS_TD )
  ));

  //Load Theme Textdomain
  load_theme_textdomain( TS_TD, TS_PATH.'/languages' );
 
}
}

/**
 * Register/Enqueue Styles & Scripts
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if( !function_exists( 'matterial_enqueue_scripts' ) ) {
function matterial_enqueue_scripts() {
  //Enqueue styles
  wp_enqueue_style( 'google-fonts-matterial', 'http://fonts.googleapis.com/css?family=Roboto:400,700,700italic,400italic,300italic,300' );  
  wp_enqueue_style( 'main-matterial', TS_CSS.'/style'.TS_MIN.'.css', array(), TS_VER );

  //RTL
  if( is_matterial_rtl() ) {
    wp_enqueue_style( 'rtl-matterial', TS_CSS.'/rtl'.TS_MIN.'.css', array(), TS_VER );
  }

  //Enqueue scripts
  wp_enqueue_script( 'plugins-matterial', TS_JS.'/jquery.plugins'.TS_MIN.'.js', array('jquery'), TS_VER, true );
  wp_enqueue_script( 'register-matterial', TS_JS.'/jquery.register'.TS_MIN.'.js', array('plugins-matterial'), TS_VER, true );

}
}

/**
 * Matterial Main Menu
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if ( !function_exists( 'matterial_main_menu' ) ) {
function matterial_main_menu() {
  //
  echo '<div class="mt-main-sidebar mt-sidebar-wrapper">
    <div class="mt-sidebar-close"></div>
    <div class="mt-sidebar-content"><a href="#">Main Sidebar</a></div>
  </div>';
}
}

/**
 * Matterial Shop Sidebar
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if ( !function_exists( 'matterial_shop_sidebar' ) ) {
function matterial_shop_sidebar() {
  //
  echo '<div class="mt-shop-sidebar mt-sidebar-wrapper">
    <div class="mt-sidebar-close"></div>
    <div class="mt-sidebar-content"><a href="#">Shop Sidebar</a></div>
  </div>';
}
}

/**
 * Page Header Before
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if( !function_exists( 'matterial_before_page_header' ) ) {
function matterial_before_page_header() {
  //
  echo '<div class="page-header-bg" data-parallax="true">
      <!--<div class="bg-video">
        <video width="320" height="240" autoplay="autoplay" muted="muted" loop="loop">
          <source type="video/mp4" src="http://slupytheme.com/wp-content/uploads/2014/06/girl-on-nature-with-the-tablet.mp4" />
          <source type="video/webm" src="http://slupytheme.com/wp-content/uploads/2014/06/girl-on-nature-with-the-tablet.webm" />
        </video>
      </div>-->
      <div class="page-header-cover"></div>
    </div>';

}
}

/**
 * Page Header After
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 *
 * Cart Icon List
 *  mticon-shopping-basket
 *  mticon-shopping-cart2
 *  mticon-add-shopping-cart
 *  mticon-store-mall-directory
 *  mticon-shop
 *  mticon-shopping-cart
 *  mticon-google-wallet
 *  mticon-cart-plus
 *  mticon-cart-arrow-down
 *
 */
if( !function_exists( 'matterial_after_page_header' ) ) {
function matterial_after_page_header() {
  //
  echo '<div class="mt-menu">
    <a href="#" class="mt-btn shop-menu-btn mt-btn-circle mt-btn-blue mt-floating-btn mticon-shopping-cart2"></a>
  </div>';

}
}

/**
 * Breadcrumbs Plugin Support
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if( !function_exists( 'matterial_breadcrumbs' ) ) {
function matterial_breadcrumbs() {
  //container
  $before = '<div id="mt-breadcrumbs" class="breadcrumbs">';
  $after = '</div><!-- .breadcrumbs -->';
  $separator = '<span class="mt-sep-arrow"></span>';

  if( is_woocommerce_activated() && is_woocommerce() ){
    //WooCommerce Breadcrumb
    woocommerce_breadcrumb( array(
      'delimiter'   => $separator,
      'wrap_before' => '<nav id="mt-breadcrumbs" class="breadcrumbs" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>',
      'wrap_after'  => '</nav><!-- .breadcrumbs -->',
      'before'      => '<span>',
      'after'       => '</span>',
    ));
  }else if( function_exists('yoast_breadcrumb') ) {
    //Yoast SEO Breadcrumb
    yoast_breadcrumb($before, $after);
  }else if( function_exists('breadcrumb_trail') ) {
    //Breadcrumb Trail
    breadcrumb_trail( array(
      'show_browse' => false,
      'separator'   => $separator
    ));
  }else if( function_exists('bcn_display') ) {
    //Breadcrumb Navxt
    echo $before;
    bcn_display();
    echo $after;
  }

}
}

/**
 * Header Logo
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if( !function_exists( 'matterial_header_logo' ) ) {
function matterial_header_logo() {
  // get home url
  $home_url = esc_url( get_home_url() );

  echo '<div class="mt-header-logo text-center">';
    echo '<a href="'.$home_url.'"><img src="'.esc_url(get_template_directory_uri().'/images/logo.png').'" alt="'.__('Logo', TS_TD ).'"></a>';
    //echo '<h1><a href="'.$home_url.'">'.get_bloginfo( 'name' ).'</a></h1>';
  echo '</div><!-- .mt-header-logo -->';

}
}

/**
 * Yoast SEO Breadcrumbs Plugin Separator
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if( !function_exists( 'matterial_breadcrumbs_separator' ) ) {
function matterial_breadcrumbs_separator() {
  //separator
  $separator = '<span class="mt-sep-arrow"></span>';

  return $separator;
}
}

/**
 * WooCommerce Status
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if ( !function_exists( 'is_woocommerce_activated' ) ) {
function is_woocommerce_activated() {
  //check woocommerce class
  return class_exists( 'woocommerce' ) ? true : false;
}
}

/**
 * Matterial RTL
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if ( !function_exists( 'is_matterial_rtl' ) ) {
function is_matterial_rtl() {
  //check rtl
  return is_rtl() ? true : false;
}
}


/**
 * Include Matterial Part
 * 
 * @since 1.0
 * @author ThemeSama (@theme_sama)
 */
if ( !function_exists( 'get_matterial_part' ) ) {
function get_matterial_part( $part = 'pageheader' ) {
  //
  get_template_part( 'template-parts/matterial', $part );
}
}