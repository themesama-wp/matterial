<?php
/**
 * The template for displaying the footer
 *
 * @since Matterial 1.0
 */
?>

  <footer class="site-footer" role="contentinfo">
    <div class="container">
      Footer
    </div>
  </footer><!-- .site-footer -->

  <div class="site-copyright">
    <?php do_action('matterial_site_copyright'); ?>
  </div><!-- .site-copyright -->

</div><!-- .site-content -->

<?php do_action('matterial_main_after'); ?>

<?php wp_footer(); ?>

</body>
</html>