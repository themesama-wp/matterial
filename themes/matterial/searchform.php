<?php
/**
 * The template for displaying Search Form
 *
 * @since Matterial 1.0
 */
?>
<form class="search" method="get" action="<?php echo esc_url( home_url() ); ?>" role="search">
  <div class="mt-input-group<?php echo is_404() ? ' mt-input-group-lg' : ''; ?>">
    <input class="search-input mt-form-control" type="search" name="s" placeholder="<?php esc_attr_e( 'What are you looking for?', 'matterial' ); ?>">
    <span class="mt-input-group-btn">
      <button class="search-submit mt-btn mt-btn-primary" type="submit" role="button"><?php _e( 'Search', 'matterial' ); ?></button>
    </span>
  </div>
  <?php
    if( defined('ICL_LANGUAGE_CODE') ){
      echo '<input type="hidden" name="lang" value="'.esc_attr( ICL_LANGUAGE_CODE ).'"/>';
    }
  ?>
</form><!-- .search-form -->