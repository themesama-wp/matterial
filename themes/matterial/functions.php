<?php
/**
 * Matterial Theme functions and definitions
 *
 * @since Matterial 1.0
 * @author ThemeSama (@theme_sama)
*/

if ( !isset( $content_width ) ) {
  $content_width = 1200;
}

defined( 'IS_MATTERIAL' )     || define( 'IS_MATTERIAL', true );
defined( 'MATTERIAL_PATH' )   || define( 'MATTERIAL_PATH', get_template_directory() );

//Framework & Framework Settings
load_template( MATTERIAL_PATH.'/includes/matterial-settings.php' );
load_template( MATTERIAL_PATH.'/ts-framework/admin.php' );

//Matterial Functions & Hooks
load_template( MATTERIAL_PATH.'/includes/matterial-plugins.php' );
load_template( MATTERIAL_PATH.'/includes/matterial-functions.php' );
load_template( MATTERIAL_PATH.'/includes/matterial-hooks.php' );