<?php
/**
 * The template for displaying WooCommerce products
 *
 * @since Slupy 1.0
 */

get_header();
get_matterial_part( 'pageheader' ); ?>

  <div class="page-content">
    <div class="container">
        <?php woocommerce_content(); ?>
    </div>
  </div><!-- .page-content -->

<?php get_footer(); ?>