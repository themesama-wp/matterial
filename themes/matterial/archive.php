<?php
/**
 * The template for displaying archive pages
 *
 * @since Matterial 1.0
 */

get_header(); ?>
  
  <header class="page-header">

    <?php do_action('matterial_before_page_header'); ?>

    <div class="<?php echo apply_filters('matterial_page_header_container_class', 'container'); ?>">
      <div class="row">

        <div class="col-xs-12 page-header-standard">
          <?php
            the_archive_title( '<h1 class="page-title">', '</h1>' );
            the_archive_description( '<p class="page-description">', '</p>' );
          ?>
        </div>

      </div>
    </div>

    <?php do_action('matterial_after_page_header'); ?>

  </header><!-- .page-header -->


<?php get_footer(); ?>