;(function ( $, window, document, undefined ) {
  'use strict';

  var $window = $(window),
      $document = $(document),
      window_height = $window.height(),
      is_screen,
      is_device_mobile = (navigator.userAgent.match(/(Android|iPhone|iPad|iPod|Opera Mini|webOS|BlackBerry|IEMobile)/)) ? true : false;


  /**
   * Exists Element
   *
   * @since 1.0
   */
  $.exists = function(selector) {
    return ($(selector).length > 0);
  };

  /**
   * Viewport Bootstrap
   *
   * @since 1.0
   */
  $.viewportBS = function() {
    if( $window.width() < 768 ) {
      is_screen = 'xs';
    }else if ( $window.width() >= 768 && $window.width() <= 992 ) {
      is_screen = 'sm';
    }else if( $window.width() > 992 && $window.width() <= 1200 ) {
      is_screen = 'md';
    }else  {
      is_screen = 'lg';
    }
  }

  /**
   * Page Header
   *
   * @since 1.0
   */
  $.fn.mtPageHeader = function() {

    return this.each(function() {
      var $this   = $(this),
          $menu   = $this.find('.mt-menu'),
          $title  = $this.find('.page-title'),
          $desc   = $this.find('.page-description'),
          $bg     = $this.find('.page-header-bg'),
          $video  = $bg.find('.bg-video'),
          $btn    = $('#header-cart-btn'),
          _current_scroll,
          _page_header_height,
          _opacity;

      //video bg need remove on mobile
      if( is_device_mobile ) {
        $video.remove();
      }

      //bg fade effect
      $bg.hide().waitForImages(function() {
        $bg.fadeIn();
        
        if( $bg.is('[data-parallax]') ){
          $bg.mtParallaxBG();
        }
      });

      //bg video
      if( !is_device_mobile && $video.length > 0 ) {
        $video.mtVideoBG();
      }

      //floating button on menu
      $menu.waypoint(function(direction) {
        if( direction == 'up' ) {
          $btn.addClass('hidden');
        }else {
          $btn.removeClass('hidden');
        }
      });

      //page header items opacity
      function _items_opacity(){
        //Get current scroll position
        _current_scroll = $window.scrollTop();
        _page_header_height = $this.height();

        if( _page_header_height - _current_scroll != 0 ) {
          //get opacity value
          _opacity = ( _page_header_height - _current_scroll ) / _page_header_height;
          _opacity = _opacity > 1 ? 1 : _opacity;
          _opacity = _opacity < 0 ? 0 : _opacity;

          //set opacity
          $title.css('opacity', _opacity);
          $desc.css('opacity', _opacity);
        }

      }

      //
      $window.bind('scroll', _items_opacity).bind('resize', _items_opacity);

    });
  };

  /**
   * Site Header
   *
   * @since 1.0
   */
  $.fn.mtSiteHeader = function() {

    return this.each(function() {
      var $this = $(this),
          _header_height = $this.outerHeight() * -1.2,
          _top_position = 0,
          _previous_scroll = 0,
          _current_scroll;

      //
      function _sticky_header(){
        //Get current scroll position
        _current_scroll = $window.scrollTop();

        //Check scroll position
        if( _current_scroll == 0 ) {
          _top_position = $('body').hasClass('admin-bar') ? $('#wpadminbar').height() : 0;
          $this.css('top', _top_position).removeClass('sticky-header');
        }else if( ( _current_scroll > _previous_scroll || $window.scrollTop() + window_height == $document.height() ) && !$this.hasClass('sticky-header') ) {
          $this.css('top', _header_height);
        }else if( _current_scroll < _previous_scroll && !$this.hasClass('sticky-header') ) {
          $this.addClass('sticky-header');
        }else if( _current_scroll > _previous_scroll && $this.hasClass('sticky-header') ) {
          $this.css('top', _header_height).removeClass('sticky-header');
        }

        //Get latest position
        _previous_scroll = _current_scroll;
      }

      //
      $window.bind('scroll', _sticky_header).bind('resize', _sticky_header);

    });
  };

  /**
   * Floating Menu
   *
   * @since 1.0
   */
  $.fn.mtFloatingMenu = function() {

    return this.each(function() {
      var $this = $(this);

      //
      function _visible(){
        if( $window.scrollTop() + window_height == $document.height() && (is_screen == 'xs' || is_screen == 'sm') && !$this.hasClass('hide-floating-btn') ) {
          $this.addClass('hide-floating-btn');
        }else if( $this.hasClass('hide-floating-btn') ) {
          $this.removeClass('hide-floating-btn');
        }
      }

      //
      $window.bind('scroll', _visible);

    });
  };

  /**
   * Video BG
   *
   * @since 1.0
   */
  $.fn.mtVideoBG = function() {

    return this.each(function() {
      var $this = $(this),
          $video = $this.find('video'),
          _content_width,
          _content_height,
          _video_width,
          _video_height,
          _dimension;

      //remove video container styles
      $this.removeAttr('style');

      //video fade effect
      if( !$video.hasClass('mt-fade-video') ){
        $video.hide().addClass('mt-fade-video');
      }

      //get video properties
      _content_width  = $this.parent().outerWidth();
      _content_height = $this.parent().outerHeight();
      _video_width    = $video.width();
      _video_height   = $video.height();

      //check video position
      if(_video_height < _content_height){
        _dimension = (120 * _content_height) / _video_height;
        $this.css({width: _dimension+'%', height: _content_height+'px'});
        _video_width  = $video.width();
        _video_height = $video.height();
      }

      //center video horizontal
      if(_content_width < _video_width){
        $this.css({left:'-'+((_video_width-_content_width)/2)+'px'});
      }

      //center video vertical
      if(_content_height < _video_height){
        $this.css({top:'-'+((_video_height-_content_height)/2)+'px'});
      }

      $video.fadeIn();

    });
  };

  /**
   * Parallax BG
   *
   * @since 1.0
   */
  $.fn.mtParallaxBG = function() {

    return this.each(function() {

      var $this       = $(this),
          _speed      = parseFloat($this.data('speed')) || 0.2,
          _offset     = $this.data('offset') || '50%',
          _first      = $this.offset().top;

      // function to be called whenever the window is scrolled or resized
      function _update_bg_pos(){
        var _pos = $window.scrollTop(),
            _top = $this.offset().top,
            _height = $this.height();

        // Check if totally above or totally below viewport
        if (_top + _height < _pos || _top > _pos + window_height) {
          return;
        }

        $this.css('backgroundPosition', _offset + " " + Math.round((_first - _pos) * _speed) + "px");
      }

      if( !is_device_mobile ) {
        _update_bg_pos();
        $window.bind('scroll', _update_bg_pos).resize(_update_bg_pos);
      }else {
        $this.addClass('is-mobile-background');
      }

    });

  };

  /**
   * Sidebars
   *
   * @since 1.0
   */
  $.fn.mtSidebars = function() {

    return this.each(function() {

      var $this       = $(this),
          $content    = $this.find('.mt-sidebar-content'),
          $close      = $this.find('.mt-sidebar-close'),
          _btn        = $this.is('.mt-main-sidebar') ? 'main' : 'shop',
          $btn        = _btn == 'main' ? $('.main-menu-btn') : $('.shop-menu-btn');

      //open sidebars
      $btn.click(function(e){
        e.preventDefault();
        
        //check menu state
        if( !$('body').hasClass('mt-'+ _btn +'-sidebar-active') && !$this.hasClass('mt-sidebar-open') ) {
          $('body').addClass('mt-'+ _btn +'-sidebar-active').delay(200).queue(function(){
            $this.addClass('mt-sidebar-open').dequeue();
          });
        }
      });

      //close sidebars
      $close.click(function(e){
        e.preventDefault();
        
        //check menu state
        if( $('body').hasClass('mt-'+ _btn +'-sidebar-active') && $this.hasClass('mt-sidebar-open') ) {
          $this.removeClass('mt-sidebar-open').delay(400).queue(function(){
            $('body').removeClass('mt-'+ _btn +'-sidebar-active').dequeue();
          });
        }
      });
      
      //
      $content.perfectScrollbar();

    });

  };

  /**
   * Hooks
   *
   * @since 1.0
   */
  $(document).ready( function(){
    //Page Header & Site Header
    $('.site-header').mtSiteHeader();
    $('.page-header .mt-menu').mtFloatingMenu();
    $('.page-header').mtPageHeader();
    $('.mt-main-sidebar, .mt-shop-sidebar').mtSidebars();
    
    //Buttons Wave Effects
    $('.header-btn, .woocommerce-breadcrumb a, .breadcrumb-trail a, #mt-breadcrumbs a').addClass('waves-effect');
    $('.mt-btn').addClass('waves-effect waves-light');
    Waves.init();

    //Get Viewport
    $.viewportBS();
  });

  $(window).resize( function(){
    window_height = $window.height();
    $.viewportBS();
    $('.page-header-bg .bg-video').mtVideoBG();
  });

  $(window).scroll( function(){
    // do stuff
  });

  $(window).load( function(){
    // do stuff
  });

})( jQuery, window, document );